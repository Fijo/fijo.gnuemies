
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Fijo.Gnuemies
{
	public class PartyCardProvider : IPartyCardProvider	{
		protected IList<PartyCard> GetAll ()
		{
			return new List<PartyCard>	{
				new PartyCard	{
					Number = 6,
					Picture = new Picture	{
						PersonName = "",
						Image = (object) null
					}
				}
			};
		}
		public PartyCard Get()	{
			return GetAll().First();	
		}
	}
}
