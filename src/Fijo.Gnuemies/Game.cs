using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.Gnuemies
{
	public class Game	{
		IPartyCardProvider PartyCardProvider = Kernel.Resolve<IPartyCardProvider>();
		public void Period (IList<Player> players)
		{
			var partyCard = PartyCardProvider.Get ();
			players.ForEach(DisplayCurrentPlayer);
			DisplayCurrentPartyCard (partyCard);
		}
		private void DisplayCurrentPlayer (Player player)
		{
			DisplayCurrentPlayerName (player);
			DisplayCurrentPlayerCards(player);
		}
		private void DisplayCurrentPlayerName (Player player)
		{
			Console.SetCursorPosition (0, 3);
			Console.WriteLine ("Player: " + player.PlayerName);
		}
		private void DisplayCurrentPlayerCards (Player player)
		{
			Console.SetCursorPosition (0, 4);
			Console.WriteLine ("PlayerCards: " + string.Join (" ", 
				player.PlayerCards.Select (x => "[ " + x.Points + " ( " + x.SubPoints + " ) ]")));
		}
        public void PeriodEnd (PartyCard partyCard, IDictionary<Player, PlayerCard> playerCards)
		{
			var ordered = playerCards.OrderBy (x => x.Value.Points).ThenBy(x => x.Value.SubPoints);
			var winner = GetWinnerResolverFunc<KeyValuePair<Player, PlayerCard>> (partyCard) (ordered);
			Console.WriteLine(winner.Value);
        }
        private Func<IEnumerable<T>, T> GetWinnerResolverFunc<T>(PartyCard partyCard)
        {
            switch (partyCard.Number)
            {
                case PartyCard.WullewaukiNumber:
                    return x => x.First();
                default:
                    return x => x.Last();
            }
        }
        private void DisplayCurrentPartyCard(PartyCard partyCard)   {
			Console.SetCursorPosition (0, 1);
            Console.WriteLine(partyCard.Number);
        }
	}
}