using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;


namespace Fijo.Gnuemies
{
    public class GnuemieInjectionModule : IExtendedNinjectModule {
        public IKernel Kernel { get; private set; }
    	public IDictionary<IExtendedInitHandler, object> ExtendedInit { get; set; }

    	public void AddModule(IKernel kernel) {}

    	public void OnLoad (IKernel kernel)
		{
			Kernel = kernel;
			
			kernel.Bind<IPartyCardProvider> ().To<PartyCardProvider> ().InSingletonScope ();
			Kernel.Bind<PlayerFactory>().ToSelf().InSingletonScope();
		}

		public void Init (IKernel kernel)
		{
		}

		public void RegisterHandlers (IKernel kernel)
		{
		}

		public void Validate ()
		{
		}
		
    	public void Loaded(IKernel kernel) {}

        public void OnUnload(IKernel kernel) {}

    	public string Name
        {
            get { return "GnuemieInjectionModule"; }
        }
    }
}
