
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Fijo.Gnuemies
{
	public class PartyCard	{
		public int Number;
        public Picture Picture;
        public const int WullewaukiNumber = -1;
        public const int EvoluzerNumber = 0;
        public static PartyCard Wullewauki
        {
            get
            {
                return new PartyCard    {
                    Number = WullewaukiNumber,
                    Picture = null
                };
            }
        }
        public static PartyCard Evoluzer
        {
            get
            {
                return new PartyCard    {
                    Number = 0,
                    Picture = null
                };
            }
        }
	}
}
