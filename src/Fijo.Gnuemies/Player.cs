
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Fijo.Gnuemies
{
	public class Player	{
		public string PlayerName;
		public IList<PlayerCard> PlayerCards;
		public IList<PartyCard> PartyCards;
	}
}
