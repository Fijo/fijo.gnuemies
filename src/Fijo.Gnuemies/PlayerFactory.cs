
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
namespace Fijo.Gnuemies
{
	public class PlayerFactory	{
		public Player CreatePlayer (string name)
		{
			return new Player	{
				PartyCards = new List<PartyCard>(),
				PlayerCards = new List<PlayerCard>(),
				PlayerName = name
			};
		}
	}
}
