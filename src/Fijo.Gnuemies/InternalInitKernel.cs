using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.LightContrib.Properties;

namespace Fijo.Gnuemies
{
	public class InternalInitKernel : ExtendedInitKernel
	{
		public override void PreInit()
		{
			LoadModules(new LightContribInjectionModule(), new GnuemieInjectionModule());
		}
	}
}