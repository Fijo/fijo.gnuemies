using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Fijo.Gnuemies
{
	public interface IPartyCardProvider	{
		PartyCard Get();
	}
}
